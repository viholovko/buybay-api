def seed_roles
 Role.destroy_all

 roles_attributes = [
  { name: :admin },
  { name: :client },
 ]

 roles_attributes.each do |role|
  Role.create role
 end
end
