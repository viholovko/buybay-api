def seed_categories
 Category.destroy_all

 10.times { |i| Category.create(name: "Category #{i}") }
 
end
