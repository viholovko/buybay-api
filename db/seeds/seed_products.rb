def seed_products
 Product.destroy_all

 10.times { |i| Product.create(name: "Product #{i}", code: "000#{i}",
  available_count: rand(0..10), category: Category.order('RANDOM()').first, parent_id: randomProduct()) }
 
end

def randomProduct()
  if Product.count != 0
    Product.order('RANDOM()').first.id
  end
end
