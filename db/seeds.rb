Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }

seed_roles
seed_admin
seed_categories
seed_products
