class CreateProduct < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.float :price, default: 0
      t.integer :available_count
      t.integer :category_id
      t.boolean :active, default: false
      t.references :parent, index: true
      
      t.timestamps
    end
    
    add_index :products, :code, unique: true
  end
end
