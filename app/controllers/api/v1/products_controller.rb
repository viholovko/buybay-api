class Api::V1::ProductsController < Api::V1::BaseController

  load_and_authorize_resource :Product

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Product.search_query params
    count_query = Product.search_query params.merge(count: true)
    max_query = Product.search_query params.merge(max: true)
    min_query = Product.search_query params.merge(min: true)

    @products = Product.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Product.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
    @max = Product.find_by_sql(max_query.to_sql).first.try(:[], 'max')
    @min = Product.find_by_sql(min_query.to_sql).first.try(:[], 'min')
  end

  def show
    @product = Product.includes(:category, :parent, :children).active.find(params[:id])
  end
end
