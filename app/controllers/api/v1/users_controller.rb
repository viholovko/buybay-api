class Api::V1::UsersController < Api::V1::BaseController

  load_and_authorize_resource :user, only: :show
  skip_before_action :authenticate_user, only: [:create]

  def create
    @user = User.new create_params

    @user.role = Role.get_client

    if @user.save
      sign_in user: @user

      render json: { session_token: current_session.token }
    else
      render json: { errors: @user.errors }, status: :bad_request
    end
  end

  def profile
    @user = current_user

    if @user.present?
      render json: { user: @user.to_json }
    else
      render json: { errors: @user.errors }, status: :bad_request
    end
  end

  def update
    @user = current_user

    if @user.update update_params
      render json: { user: @user.to_json, message: 'Update successful' }
    else
      render json: { errors: @user.errors }, status: :bad_request
    end
  end

  def destroy
    @user = current_user
    if @user.destroy
      render json: { status: 'successful' }
    else
      render json: { errors: @user.errors }, status: :bad_request
    end
  end

  private

  def create_params
    params.permit :first_name, :last_name, :email, :password
  end

  def update_params
    allowed_params = params.permit :first_name, :last_name, :avatar
    allowed_params[:avatar] = nil if allowed_params[:avatar] == "null"

    allowed_params
  end

end