class Api::V1::CategoriesController < Api::V1::BaseController

  load_and_authorize_resource :category

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Category.search_query params
    count_query = Category.search_query params.merge(count: true)

    @categories = Category.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Category.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def show
    @category = Category.find(params[:id])
  end

end
