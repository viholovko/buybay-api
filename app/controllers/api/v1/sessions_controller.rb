class Api::V1::SessionsController < Api::V1::BaseController

  skip_before_action :authenticate_user, except: [:destroy]

  def create
    @user = User.find_by(email: params[:email])

    if @user&.authenticate params[:password]
      sign_in user: @user

      render json: { session_token: current_session.token, user: @user.to_json }
    else
      render json: { errors: [{ message: "Email or password is wrong" }] }, status: :bad_request
    end
  end

  def destroy
    sign_out
    render json: { message: 'Logout successful' }
  end
end