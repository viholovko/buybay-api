class Admin::ProductsController < Admin::BaseController

  load_and_authorize_resource :product

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = Product.search_query params
    count_query = Product.search_query params.merge(count: true)

    @products = Product.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = Product.find_by_sql(count_query.to_sql).first.try(:[], 'count').to_i
  end

  def create
    @product = Product.new product_params

    if @product.save
      render json: { product: @product, message: I18n.t('messages.success_upsert') }
    else
      render json: {validation_errors: @product.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @product.update product_params
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @product.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @product.destroy
    render json: { message: I18n.t('messages.success_destroy') }
  end

  def show
    @product = Product.includes(:category, :parent, :children).find(params[:id])
  end

  private

  def product_params
    params.permit :name, :code, :available_count, :active, :price, :parent, :category_id
  end

end
