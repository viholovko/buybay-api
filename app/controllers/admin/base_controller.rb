class Admin::BaseController < ApplicationController

  before_action :only_admins

  before_action :set_default_response_format
  skip_before_action :verify_authenticity_token

  protected

  def set_default_response_format
    request.format = :json
  end

  rescue_from Exception, with: :catch_exceptions

  def authenticate_user
    render json: {errors: [{ "message": "Access denied." }] }, status: :bad_request and return unless current_session
  end

  private

  def catch_exceptions(e)
    logger.error e.message
    logger.error e.backtrace.join("\n")

    case e.class.name
    when 'ActiveRecord::RecordNotFound'
      render json: { errors: [{ "message": "Record not found." }] }, status: :bad_request
    when 'ArgumentError'
      render json: { errors: [e.message] }, status: :unprocessable_entity
    when 'ActiveRecord::StatementInvalid'
      render json: { errors: [e.message] }, status: :unprocessable_entity
    when 'ActiveRecord::RecordInvalid'
      render json: { errors: [e.message] }, status: :unprocessable_entity
    when 'CanCan::AccessDenied'
      render json:{ errors: [e.message] }, status: 403
    else
      render json: { errors: [{ "message": "Unhandled error." }] }, status: :bad_request
    end
  end

  def only_admins
    render json: { errors: ['Access denied !'] }, status: :unauthorized and return unless current_user&.admin?
  end
end