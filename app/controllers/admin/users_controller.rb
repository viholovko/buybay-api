class Admin::UsersController < Admin::BaseController

  load_and_authorize_resource :user

  def index
    page = params[:page].to_i
    page = 1 if page < 1
    per_page = params[:per_page].to_i
    per_page = 10 if per_page < 1

    query = User.search_query search_params
    count_query = User.search_query search_params.merge count: true

    @users = User.find_by_sql(query.take(per_page).skip((page - 1) * per_page).to_sql)
    @count = User.find_by_sql(count_query.to_sql).first['count']
  end

  def create
    @user = User.new user_params

    if @user.save
      render json: { message: I18n.t('messages.success_upsert') }
    else
      render json: { validation_errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      render json: { message: I18n.t('messages.success_destroy') }
    else
      render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def show
    @user = User.includes(:role).find(params[:id])
  end

  def profile
    @user = current_user
  end

  private

  def user_params
    allowed_params = params.permit :email, :password, :password_confirmation, :first_name, :last_name, :avatar
    allowed_params[:role_id] = Role.send("get_#{ params[:role].present? ? params[:role] : 'admin' }").id
    allowed_params
  end

  def search_params
    allowed_params = params.permit :sort_column, :sort_type, :name, roles: []
    allowed_params[:current_user] = current_user
    allowed_params
   end
 
end