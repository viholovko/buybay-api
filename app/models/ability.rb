class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    can :manage, :all if user.admin?

    not_upgraded_abilities(user) if user.not_upgraded?

    client_abilities(user) if user.client?
  end

  def not_upgraded_abilities(user)
    can :index, User
    can :show, User
  end

  def client_abilities(user)
    can :profile, User
    can :destroy, User
    can [:index, :show], Category
    can [:index, :show], Product
  end
end
