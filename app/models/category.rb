class Category < ApplicationRecord
  validates :name, presence: { message: {message: "Name can't be blank." } },
                   uniqueness: { message: 'This name is already registered.'}

  class << self
    def search_query(params)
      categories = Category.arel_table

      q = categories.project(params[:count] ? "COUNT(*)" : Arel.star)
  
      if params[:count]
      else
        if Category.column_names.include?(params[:sort_column]) && %w(asc desc).include?(params[:sort_type])
          q.order(categories[params[:sort_column]].send(params[:sort_type] == "asc" ? :asc : :desc))
        else
          q.group(categories[:id])
          q.order(categories[:id].desc)
        end
      end
  
      q.where(categories[:name].matches("%#{params[:name]}%")) if params[:name].present?
  
      q
    end

  end
end

