class Session < ApplicationRecord
  belongs_to :user
  before_create :generate_token
  before_create :remove_expired_sessions
  validates :user_id, presence: true

  enum device_type: {
    android: 0,
    ios: 1
  }

  def self.destroy_expired
    where("updated_at < ?", Time.now - 1.month).destroy_all
  end

  private

  def generate_token
    self.user.update_attribute :last_logged_in, Time.now
    self.token = encrypt
  end

  def encrypt
    secure_hash("#{ Time.now.utc - (rand(1000).hours) }--#{ self.user.email }--#{ self.user.salt }")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  def remove_expired_sessions
    Session.destroy_expired
  end
end