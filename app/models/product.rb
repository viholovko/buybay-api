class Product < ApplicationRecord
  belongs_to :category, required: false

  has_many :children, class_name: "Product", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Product", required: false

  validates :code, presence: { message: { message: "Code can't be blank." } },
            uniqueness: { message: 'This code is already registered.' }

  validates :name, presence: { message: { message: "Name can't be blank." } }

  before_save :check_available_status, if: :available_count_changed?

  scope :active, -> { where active: true }

  class << self
    def query(params)
      products = Product.arel_table
      categories = Category.arel_table
      parents = Product.arel_table.alias('parents')

      q = products
            .project(
              'products.*',
              "json_build_object('id', categories.id, 'name', categories.name) AS category",
              "json_build_object('id', parents.id, 'name', parents.name, 'code', parents.code, 'active', parents.active) AS parent",
              "(SELECT json_agg(child) FROM (SELECT * FROM products AS children WHERE children.parent_id = products.id ORDER BY children.id ASC) child ) AS children"
            ).group(products[:id], categories[:id], parents[:id])

      q.join(categories, Arel::Nodes::OuterJoin).on(products[:category_id].eq(categories[:id]))
      q.join(parents, Arel::Nodes::OuterJoin).on(products[:parent_id].eq(parents[:id]))

      q.where(products[:id].eq(params[:id])) if params[:id].present?
      q.where(products[:name].matches("%#{params[:name]}%")) if params[:name].present?
      q.where(products[:code].matches("%#{params[:code]}%")) if params[:code].present?

      q.where(products[:parent_id].in(params[:parent_id])) if params[:parent_id].present?
      if params[:price_from].present? && params[:price_to].present?
        q.where(products[:price].between(params[:price_from]..params[:price_to]))
      elsif params[:price_from].present? && !params[:price_to].present?
        q.where(products[:price].gteq(params[:price_from]))
      elsif !params[:price_from].present? && params[:price_to].present?
        q.where(products[:price].lteq(params[:price_to]))
      end

      q
    end

    def search_query(params)
      products = Product.arel_table

      q = query(params).as('t')

      result = if params[:count]
                 products.project("COUNT(*)").from(q)
               elsif params[:max]
                 products.project("max(price)").from(q)
               elsif params[:min]
                 products.project("min(price)").from(q)
               else
                 products.project("t.*").from(q).order(q[:id].desc)
               end
      result
    end
  end

  private

  def check_available_status
    self.active = available_count != 0
  end
end

