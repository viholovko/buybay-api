class User < ApplicationRecord
  has_many :sessions, dependent: :destroy
  belongs_to :role
  attr_accessor :password, :password_confirmation
  
  validates :first_name,
      presence: { message: {message: "First name can't be blank." } },
      format: { with: /\A[a-zA-Z]+\z/,
           message: {message: "First name must be letters only" }
      }, if: -> { !admin? }
  validates :last_name,
      presence: { message: {message: "Last name can't be blank." } },
      format: { with: /\A[a-zA-Z]+\z/,
           message: {message: "Last name must be letters only" }
      }, if: -> { !admin? }
  validates :password,
      presence: true, confirmation: true, length: {within: 6..40}, if: :validate_password?
  validates :password_confirmation,
      presence: true, if: :validate_password?
  validates :email,
      uniqueness: { case_sensitive: false, message: 'This email address is already registered.'},
      format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: 'email is incorrect', on: :create },
      allow_blank: false

  before_save    :encrypt_password
  before_validation :downcase_email
  before_destroy  :validate_destroy

  Role::NAMES.each do |name_constant|
    define_method("#{name_constant}?") { self.role.try(:name) == name_constant.to_s }
  end

  def authenticate(password)
    self.encrypted_password == encrypt(password)
  end

  class << self
    def query(params)
      users = User.arel_table
      roles = Role.arel_table

      params[:role_ids] ||= []
      params[:roles] ||= []
      params[:roles].each do |role_name|
        params[:role_ids] << Role.send("get_#{ role_name }").id if Role.respond_to?("get_#{ role_name }")
      end

      q = users
          .project(
            'users.*',
            roles[:name].as('role_name')
          )
          .group(users[:id], roles[:id])

      q.join(roles, Arel::Nodes::OuterJoin).on(users[:role_id].eq(roles[:id]))
      q.where(users[:id].eq(params[:id])) if params[:id].present?
      q.where(users[:email].matches("%#{ params[:email] }%")) if params[:email].present?
      q.where(users[:first_name].matches("%#{ params[:first_name] }%")) if params[:first_name].present?
      q.where(users[:last_name].matches("%#{ params[:last_name] }%")) if params[:last_name].present?
      q.where(roles[:id].in(params[:role_ids])) if params[:role_ids].present?
      q.where(roles[:name].not_eq('admin')) if !params[:show_admin].present?

      q
    end

    def search_query(params)
      users = User.arel_table

      q = query(params).as('t')

      result = users.project(params[:count] ? "COUNT(*)" : "t.*").from(q)

      if params[:count]

      else
        result.order(q[:id].desc)
      end

      result
    end

  end

  def validate_destroy
    if self.admin? && User.where(role_id: Role.get_admin.id).count == 1
      self.errors.add :base, 'Can not remove last admin.'
      throw :abort
    end
  end

  def to_json
    {
      id: id,
      email: email,
      first_name: first_name,
      last_name: last_name,
      role: role.name
    }
  end

  private

  def validate_password?
    admin? && (new_record? || !password.nil? || !password_confirmation.nil?)
  end

  def downcase_email
    self.email = self.email.downcase if self.email
  end

  def encrypt_password
    self.salt = make_salt if salt.blank?
    self.encrypted_password = encrypt(self.password) if self.password
  end

  def encrypt(string)
    secure_hash("#{ string }--#{ self.salt }")
  end

  def make_salt
    secure_hash("#{ Time.now.utc }--#{ self.password }")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end
end