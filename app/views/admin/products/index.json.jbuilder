json.products @products.each do |product|
  json.extract! product, :id, :name, :code, :price, :available_count, :active, :created_at, :updated_at
end
json.count @count
