json.product do
  json.extract! @product, :id, :name, :code, :price, :available_count, :active,
                :created_at, :updated_at, :category, :parent, :children
end
