json.users @users.each do |user|
  json.id user.id
  json.email user.email
  json.first_name user.first_name
  json.last_name user.last_name
  # json.avatar paperclip_url user.avatar
  json.created_at user.created_at.strftime("%d-%m-%Y")
end
json.count @count