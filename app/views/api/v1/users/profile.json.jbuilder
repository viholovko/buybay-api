json.user do
  json.id @user.id
  json.email @user.email
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.avatar @user.avatar.url
  json.role @user.role.name
  json.created_at @user.created_at.strftime("%d-%m-%Y")
end