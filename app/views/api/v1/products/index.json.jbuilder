json.products @products.each do |product|
  json.extract! product, :id, :name, :code,
                :price, :created_at, :updated_at, :category, :parent, :children
end
json.count @count
json.max @max
json.min @min
