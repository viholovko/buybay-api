json.product do
  json.extract! @product, :id, :name, :code,
                :price, :created_at, :updated_at, :category, :parent, :children
end