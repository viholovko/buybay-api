json.categories @categories.each do |category|
  json.extract! category, :id, :name
end
json.count @count
