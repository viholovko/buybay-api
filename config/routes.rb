Rails.application.routes.draw do
  root to: "pages#index"

  match "api/*all" => "api/base#cors_preflight_check", :constraints => { :method => "OPTIONS" }, :via => [:options]

  scope "(:locale)" do
    namespace :admin do
      post :login, to: "sessions#create"
      delete :logout, to: "sessions#destroy"
      get :profile, to: "users#profile"
      resources :roles, only: %i[index show update create destroy]
      resources :users, only: %i[index show update create destroy]
      resources :categories, only: %i[index show update create destroy]
      resources :products, only: %i[index show update create destroy]
    end

    namespace :api do
      namespace :v1 do
        post :login, to: "sessions#create"
        post :signup, to: "users#create"
        delete :logout, to: "sessions#destroy"
        get :profile, to: "users#profile"
        put :update_profile, to: "users#update"

        resources :users, only: %i[index show destroy] do
          collection do
            get :confirm_email
            put :change_password
          end
        end

        resources :password_resets, only: %i[create update]

        resources :roles, only: %i[index]
        resources :categories, only: %i[index show]
        resources :products, only: %i[index show]
      end
    end

    resources :sessions, only: %i[create] do
      collection do
        delete :destroy
        get :check
      end
    end
  end
end
