# BuyBay API

Things you may want to cover:

* Ruby version 2.6.3

* System dependencies
```shell
gem install bundler
bundle install
```

* Configuration
Database: PostgresQL version >= 12.6

* Database creation
```shell
rails db:create
rails db:migrate
```

* Database initialization
```shell
rails db:seed
```

* How to run the test suite

```shell
bundle exec rspec
```
* SimpleCov is a code coverage analysis tool for Ruby.

```shell
[ROOT_DIRECTORY]/coverage/index.html
```

## Documents for API
Documents for Postman.
Import collection and environment files from directory:
```shell
[ROOT_DIRECTORY]/documents/postman

'Session-Token' main key for access
```

## Documents for API
Default admin credantial
```shell
login: admin@gmail.com
password: secret
```

