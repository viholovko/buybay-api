FactoryBot.define do
  factory :category do
    abc = ("a".."z").to_a.join
    sequence(:name) { |n| "Category #{abc[n]}"}
  end
end
