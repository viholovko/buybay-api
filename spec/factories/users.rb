FactoryBot.define do
  factory :user do
    abc = ("a".."z").to_a.join
    sequence(:first_name) { |n| "user#{abc[n]}"}
    sequence(:last_name) { |n| "user#{abc[n]}" }
    sequence(:email) { |n| "test_#{n}@gmail.com" }

    password { 'secret!' }
    password_confirmation { 'secret!' }

    trait :not_upgraded do
      role { Role.get_not_upgraded }
    end

    trait :client do
      role { Role.get_client }
    end

    trait :admin do
      role { Role.get_admin }
    end
  end
end
