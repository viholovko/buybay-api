FactoryBot.define do
  factory :product do
    abc = ("a".."z").to_a.join
    sequence(:code) { |n| "code_#{abc[n]}" }
    sequence(:name) { |n| "Product #{abc[n]}" }
    # sequence(:children) { |n|
    #   FactoryBot.create(:product)
    # }
    sequence(:price) { rand(0.0..1000.0) }
    sequence(:available_count) { rand(0..100) }

    category { FactoryBot.create(:category) }

  end
end
