# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
 render_views

 describe 'success ' do
  it 'create session' do
   user = create :user, :client

   post :create, params: { email: user.email, password: user.password }

   expect(response.status).to be(200)
   expect(JSON.parse(response.body)['session_token']).to_not be_nil
  end

  it 'destroy session' do
   user = create :user, :not_upgraded
   session = sign_in user: user

   delete :destroy, params: { session_token: session.token }

   expect(response.status).to be(200)
   expect(user.sessions.count).to eq(0)
  end
 end

 describe 'return error' do
  it 'wrong credantials' do
   user = create :user, :admin

   post :create, params: { email: user.email, password: 'test' }

   expect(response.status).to be(400)
   expect(JSON.parse(response.body)['errors'][0]['message']).to eq('Email or password is wrong')
  end

  it 'should render unauthorized if there is no session' do
   delete :destroy, params: { session_token: ''}

   expect(response.status).to be(400)
  end
 end
end
