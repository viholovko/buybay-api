# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views

  describe 'with success' do
    it 'for create user' do
      User.where(email: 'testuser@gmail.com').destroy_all

      post :create, params: {
        email: 'test@gmail.com',
        password: 'secret',
        password_confirmation: 'secret',
        last_name: 'First',
        first_name: 'Last'
      }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['session_token']).to_not be_nil
    end

    it 'for update user' do
      user = create :user, :client
      sign_in user: user

      put :update, params: {
        last_name: 'userupdated',
        first_name: 'userupdated'
      }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['message']).to eq('Update successful')
    end

    it 'for user profile' do
      user = create :user, :client
      sign_in user: user

      get :profile
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['user']['email']).to eq(user.email)
    end

    it 'for destroy user' do
      user = create :user, :client
      sign_in user: user

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['status']).to eq('successful')
      expect(User.where(id: user.id).count).to be 0
    end
  end

  describe 'with error' do
    it 'for validation errors in create user ' do
      post :create, params: {
        email: 'test_user@gmail.com',
        password: 'secret',
        password_confirmation: 'secret',
        last_name: 'Last',
        first_name: 'first_name'
      }

      expect(response.status).to be(400)
      expect(JSON.parse(response.body)['errors']['first_name'][0]['message']).to eq('First name must be letters only')
    end

    it 'for validation error for already registered email' do
      user = create :user, :client

      post :create, params: {
        email: user.email,
        password: 'secret',
        password_confirmation: 'secret',
        last_name: 'Last',
        first_name: 'First'
      }

      expect(response.status).to be(400)
      expect(JSON.parse(response.body)['errors']['email'][0]).to eq('This email address is already registered.')
    end

    it 'for validation errors in update' do
      user = create :user, :client
      sign_in user: user

      put :update, params: {
        last_name: '',
        first_name: ''
      }

      expect(response.status).to be(400)
    end

    it 'for destroy user' do
      user = create :user, :client

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 400
      expect(JSON.parse(response.body)['errors'][0]['message']).to eq('Access denied.')
      expect(User.where(id: user.id).count).to be 1
    end
  end
end
