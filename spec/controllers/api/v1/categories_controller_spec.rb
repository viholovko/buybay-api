require 'rails_helper'

RSpec.describe ::Api::V1::CategoriesController, type: :controller do
  render_views

  before(:all) do
    Category.destroy_all
  end

  describe 'with success' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :client }

    it 'for show' do
      sign_in user: user

      category = create :category

      get :show, params: { id: category.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['category']['name']).to eq(category.name)
      expect(JSON.parse(response.body)['category']['id']).to eq(category.id)
    end

    it 'for index' do
      sign_in user: user

      create :category, name: 'Category 1'
      create :category, name: 'Category 2'

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(2)
      expect(JSON.parse(response.body)['count']).to eq(2)
    end

    it 'for index by name' do
      sign_in user: user

      category = create :category

      get :index, params: { name: category.name }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(1)
    end

    it 'for index paginate' do
      create(:category, name: 'Ruby1')
      create(:category, name: 'Ruby2')

      sign_in user: user

      get :index, params: { per_page: 1, page: 1 }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(2)
    end

    it 'for index and name not exist' do
      sign_in user: user

      get :index, params: { name: "NoName" }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(0)
      expect(JSON.parse(response.body)['count']).to eq(0)
    end
  end

  describe 'for error' do
    let(:user) { create :user, :not_upgraded }
    let(:response_body) { JSON.parse(response.body) }

    it 'for show' do
      category = create :category

      get :show, params: { id: category.id }

      expect(response.status).to be 400
    end

    it 'for show' do
      get :show, params: { id: -100 }

      expect(response.status).to be 400
    end
  end
end
