require 'rails_helper'

RSpec.describe ::Admin::ProductsController, type: :controller do
  render_views

  describe 'with success' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :admin }
    let(:category) { create :category }
    let(:parent_product) { create :product }

    it 'for create' do
      sign_in user: user

      post :create, params: { name: 'Product test', code: '0001', available_count: 1, price: 21.1, category_id: category.id }
      expect(response.status).to be 200
      product = Product.find_by(name: 'Product test')

      expect(product.category_id).to eq(category.id)
      expect(product.active).to be true
    end

    it 'validation on create' do
      sign_in user: user

      post :create, params: { name: '', code: '', available_count: 1, price: 21.1, category_id: category.id }

      expect(response.status).to be 422
      expect(JSON.parse(response.body)['validation_errors']['code'][0]['message']).to eq('Code can\'t be blank.')
      expect(JSON.parse(response.body)['validation_errors']['name'][0]['message']).to eq('Name can\'t be blank.')
    end

    it 'for update' do
      sign_in user: user

      product = create :product

      put :update, params: { id: product.id, name: 'Updated name' }

      expect(response.status).to be 200
      expect(Product.find(product.id).name).to eq('Updated name')
    end

    it 'for destroy' do
      sign_in user: user

      product = create :product

      delete :destroy, params: { id: product.id }

      expect(response.status).to be 200
      expect(Product.find_by(id: product.name)).to be_nil
    end

    it 'for show' do
      sign_in user: user

      product = create :product

      get :show, params: { id: product.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['product']['name']).to eq(product.name)
      expect(JSON.parse(response.body)['product']['code']).to eq(product.code)
    end

    it 'for index' do
      sign_in user: user

      create :product, name: 'Product 1', category: category
      create :product, name: 'Product 2', category: category

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['products'].count).to eq(2)
      expect(JSON.parse(response.body)['count']).to eq(2)
    end

    it 'for index by name' do
      sign_in user: user

      product = create :product, category: category
      create :product, name: 'Product 1', category: category
      create :product, name: 'Product 2', category: category

      get :index, params: { name: product.name }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['products'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(1)
    end

    it 'for index paginate' do
      sign_in user: user

      create :product, name: 'Product 1', category: category
      create :product, name: 'Product 2', category: category

      get :index, params: { per_page: 1, page: 2 }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['products'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(2)
    end

    it 'for index and name not exist' do
      sign_in user: user

      get :index, params: { name: "NoName" }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['products'].count).to eq(0)
      expect(JSON.parse(response.body)['count']).to eq(0)
    end
  end

  describe 'for error' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :admin }
    let(:category) { create :category }
    let(:parent_product) { create :product }

    it 'for create not authorized user' do
      post :create, params: { name: 'Product test', code: '0001', available_count: 1, price: 21.1 }
      expect(response.status).to be 400
    end

    it 'for show' do
      get :show, params: { id: -100 }

      expect(response.status).to be 400
    end

    it 'for show' do
      sign_in user: user
      get :show, params: { id: (parent_product.id + 100) }

      expect(JSON.parse(response.body)['errors'][0]['message']).to eq('Record not found.')
    end
  end
end
