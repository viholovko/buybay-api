require 'rails_helper'

RSpec.describe ::Admin::CategoriesController, type: :controller do
  render_views

  describe 'with success' do
    let(:response_body) { JSON.parse(response.body) }
    let(:user) { create :user, :admin }

    it 'for create' do
      sign_in user: user

      post :create, params: { name: 'Category test' }

      expect(response.status).to be 200
    end

    it 'validation on create' do
      sign_in user: user

      category = create :category

      post :create, params: { name: category.name }

      expect(response.status).to be 422
      expect(JSON.parse(response.body)['validation_errors']['name']).to eq(['This name is already registered.'])
    end

    it 'for update' do
      sign_in user: user

      category = create :category

      put :update, params: { id: category.id, name: 'Updated name' }

      expect(response.status).to be 200
      expect(Category.find(category.id).name).to eq('Updated name')
    end

    it 'for destroy' do
      sign_in user: user

      category = create :category

      delete :destroy, params: { id: category.id }

      expect(response.status).to be 200
      expect(Category.find_by(id: category.name)).to be_nil
    end

    it 'for show' do
      sign_in user: user

      category = create :category

      get :show, params: { id: category.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['category']['name']).to eq(category.name)
      expect(JSON.parse(response.body)['category']['id']).to eq(category.id)
    end

    it 'for index' do
      sign_in user: user

      3.times { |i| create :category }

      get :index

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(3)
      expect(JSON.parse(response.body)['count']).to eq(3)
    end

    it 'for index by name' do
      sign_in user: user

      category = create :category
      3.times { |i| create :category }

      get :index, params: { name: category.name }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(1)
    end

    it 'for index paginate' do
      sign_in user: user

      6.times { |i| create :category }

      get :index, params: { per_page: 5, page: 2 }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(1)
      expect(JSON.parse(response.body)['count']).to eq(6)
    end

    it 'for index and name not exist' do
      sign_in user: user

      get :index, params: { name: "NoName" }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['categories'].count).to eq(0)
      expect(JSON.parse(response.body)['count']).to eq(0)
    end
  end

  describe 'for error' do
    let(:user) { create :user, :not_upgraded }
    let(:response_body) { JSON.parse(response.body) }

    it 'for create not authorized user' do
      post :create
      expect(response.status).to be 400
      expect(response_body['errors'][0]['message']).to eq('Access denied.')
    end

    it 'for show' do
      category = create :category

      get :show, params: { id: category.id }

      expect(response.status).to be 400
    end

    it 'for show' do
      get :show, params: { id: -100 }

      expect(response.status).to be 400
    end

    it 'for destroy' do
      category = create :category

      delete :destroy, params: { id: category.id }

      expect(response.status).to be 400
    end

    it 'for destroy' do
      delete :destroy, params: { id: -100 }

      expect(response.status).to be 400
    end

    it 'for index' do
      3.times { |i| create :category }

      get :index

      expect(response.status).to be 400
    end
  end
end
