# frozen_string_literal: true

require 'rails_helper'
require 'benchmark'

RSpec.describe Admin::UsersController, type: :controller do
  render_views

  describe 'with success' do
    it 'for create user' do
      User.where(email: 'testuser@gmail.com').destroy_all

      user = create :user, :admin
      sign_in user: user

      post :create, params: {
        email: 'test@gmail.com',
        password: 'secret',
        password_confirmation: 'secret',
        last_name: 'First',
        first_name: 'Last'
      }

      expect(response.status).to be(200)
      expect(JSON.parse(response.body)['message']).to eq('Successfully saved.')
    end

    it 'for user profile' do
      user = create :user, :admin
      sign_in user: user

      get :profile
      expect(response.status).to be 200
      expect(JSON.parse(response.body)['user']['email']).to eq(user.email)
    end

    it 'for list of users' do
      user1 = create :user, :client
      user2 = create :user, :client
      user3 = create :user, :client

      admin = create :user, :admin
      sign_in user: admin

      get :index, params: { role: 'client' }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 3
      expect(response_body['count']).to be 3
    end

    it 'for list of users' do
      user1 = create :user, :client
      user2 = create :user, :client
      user3 = create :user, :client

      admin = create :user, :admin
      sign_in user: admin

      get :index, params: { roles: ['client'] }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['users'].count).to be 3
      expect(response_body['count']).to be 3
    end

    it 'for show of user' do
      user = create :user, :client

      admin = create :user, :admin
      sign_in user: admin

      get :show, params: { id: user.id }

      response_body = JSON.parse(response.body)

      expect(response.status).to be 200
      expect(response_body['user']['id']).to be user.id
      expect(response_body['user']['first_name']).to eq(user.first_name)
    end

    it 'for destroy user' do
      admin = create :user, :admin
      sign_in user: admin

      user = create :user, :client

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['message']).to eq('Successfully removed.')
      expect(User.where(id: user.id).count).to be 0
    end

    it 'for destroy admin' do
      admin = create :user, :admin
      sign_in user: admin

      user = create :user, :admin

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 200
      expect(JSON.parse(response.body)['message']).to eq('Successfully removed.')
      expect(User.where(id: user.id).count).to be 0
    end
  end

  describe 'with error' do
    it 'for validation errors in create user ' do
      admin = create :user, :admin
      sign_in user: admin

      post :create, params: {
        email: 'test_user@gmail.com',
        password: 'secret',
        password_confirmation: 'secret',
        last_name: 'Last',
        first_name: 'first_name',
        role: 'client'
      }

      expect(response.status).to be(422)
      expect(JSON.parse(response.body)['validation_errors']['first_name'][0]['message'])
        .to eq('First name must be letters only')
    end

    it 'for validation error for already registered email' do
      user = create :user, :client

      admin = create :user, :admin
      sign_in user: admin

      post :create, params: {
        email: user.email,
        password: 'secret',
        password_confirmation: 'secret',
        last_name: 'Last',
        first_name: 'First'
      }

      expect(response.status).to be(422)
      expect(JSON.parse(response.body)['validation_errors']['email'][0]).to eq('This email address is already registered.')
    end

    it 'for destroy user' do
      user = create :user, :client

      delete :destroy, params: { id: user.id }

      expect(response.status).to be 400
      expect(JSON.parse(response.body)['errors'][0]['message']).to eq('Access denied.')
    end

    it 'for destroy all admins' do
      User.includes(:role).where(role: { name: 'admin' }).destroy_all

      admin = create :user, :admin
      sign_in user: admin

      delete :destroy, params: { id: admin.id }

      expect(response.status).to be 422
      expect(JSON.parse(response.body)['errors'][0]).to eq('Can not remove last admin.')
    end

    it 'for user profile' do
      user = create :user, :admin
      user1 = create :user, :admin
      sign_in user: user

      User.find(user.id).destroy

      get :profile
      expect(response.status).to be 400
    end
  end
end
